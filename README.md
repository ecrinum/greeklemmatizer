# Greek Lemmatizer

Greeklemmatizer is a very simple lemmatizer for ancient Greek. It is based on Morpheus and on the Unique Tokens made available by [Giuseppe Celano here](https://github.com/gcelano/LemmatizedAncientGreekXML).

To the Unique Tokens of Celano have been added the forms found on Morpheus (by updating the dictionary produced from Morpheus with the dictionary of the Unique Tokens to keep the tokenization when existing).

It requires `gensim`, `json` and `string`.

Examples of use are available on the jupyter-notebook.



## TODO

It would be necessary to find some statistics about word frequency in order to choose the most frequent lemma.
