import json
import gensim
import string

with open('data/morpheus.json', 'r') as file:
    morpheus = json.load(file)

with open('data/uniquetokens_full.json', 'r') as f:
    morphdict= json.load(f)    

def lemma(word):
    lemmas = []
    try:
        lemmas.append({'lemma': morphdict[word], 'lemma_no_diac': None, 'morph':None, 'translation':None , 'error':None})
    except KeyError:
        lemmas=[]
    for i in morpheus['d']['t']:
        if i['f'] == word:
            lemmas.append({'lemma': i['l'], 'lemma_no_diac': i['e'], 'morph':i['p'], 'translation': i['s'], 'error':None})

    #if the form does not exist in morpheus, the fonction looks for it without diacritics and if it does not work, lemma=form
    if len(lemmas) <1:
        word_deacc = gensim.utils.deaccent(word)   
        for i in morpheus['d']['t']:
            if i['b'] == word_deacc:
                lemmas.append({'lemma': i['l'], 'lemma_no_diac': i['e'], 'morph':i['p'], 'translation': i['s'], 'error': 'found without diacritics'})
    if len(lemmas) <1:
        lemmas.append({'lemma': word, 'lemma_no_diac': word_deacc, 'morph':None, 'translation': None, 'error': 'not found'})
         
    return lemmas

def remove_punct(text):
    return text.translate(str.maketrans('', '', string.punctuation))    


class WordLemmatizer:
    def __init__(self, word):
        self.word = word
        self.lemma = list(set([lemma['lemma'] for lemma in lemma(word)]))
        self.lemma_no_diacritics = [lemma['lemma_no_diac'] for lemma in lemma(word)]
        self.morphology = [lemma['morph'] for lemma in lemma(word)]
        self.translation = [lemma['translation'] for lemma in lemma(word)]
        self.errors = list(set([lemma['error'] for lemma in lemma(word)]))
        if len(self.lemma) >1:
            self.errors.append('many lemmas')
        
class TextLemmatizer:
    def __init__(self,text):
        lemmas = []
        for word in remove_punct(text).split():
            lemmas.append((word,WordLemmatizer(word)))
        self.rich_lemmas = lemmas    
        self.lemmas = [mylemma[1].lemma for mylemma in lemmas]
        self.morphology = [mylemma[1].morphology for mylemma in lemmas]
        self.translation = [mylemma[1].translation for mylemma in lemmas]
        self.errors = [mylemma[1].errors for mylemma in lemmas]
        self.word_and_lemmas = [(mylemma[0],mylemma[1].lemma) for mylemma in lemmas]

def quicklemmatizer(text):
    lemmas=[]
    for word in remove_punct(text).split():
        try:
            lemmas.append(morphdict[word])
        except KeyError:
            #print('oi',word) #faudrait juste ajouter la recherche sans accents
            lemmas.append(WordLemmatizer(word).lemma[0])
    return lemmas   

def quicklemmatizer2(text):
    lemmas=[]
    for word in remove_punct(text).split():
        lemmas.append(lemma(word)[0]['lemma'])
    return lemmas    
